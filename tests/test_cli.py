import unittest
from pathlib import Path

import pytest
from click.testing import CliRunner

from leodagan import leodagan

DATA_VALID = Path("tests/valid")
DATA_INVALID = Path("tests/non-valid")


class TestKeywords(unittest.TestCase):
    cli_runner: CliRunner

    @classmethod
    def setup_class(cls):
        cls.cli_runner = CliRunner()

    def test_run_cli_help(self):
        out = self.cli_runner.invoke(leodagan, args=["--help"])
        assert out.exit_code == 0

    def test_run_path_not_found(self):
        out = self.cli_runner.invoke(leodagan, args=["-f", "abc"])
        assert out.exit_code == 1

    def test_run_cli_one_file_valid(self):
        args = [
            "-f",
            str(DATA_VALID / "1"),
        ]
        out = self.cli_runner.invoke(leodagan, args=args)
        assert out.exit_code == 0

    def test_run_cli_two_file_valid(self):
        args = [
            "-f",
            str(DATA_VALID / "1"),
            "-f",
            str(DATA_VALID / "2"),
        ]
        out = self.cli_runner.invoke(leodagan, args=args)
        assert out.exit_code == 0

    def test_run_cli_one_file_invalid(self):
        args = [
            "-f",
            str(DATA_INVALID / "1"),
        ]
        out = self.cli_runner.invoke(leodagan, args=args)
        assert out.exit_code == 2

    def test_run_cli_two_file_invalid(self):
        args = [
            "-f",
            str(DATA_INVALID / "1"),
            "-f",
            str(DATA_INVALID / "2"),
        ]
        out = self.cli_runner.invoke(leodagan, args=args)
        assert out.exit_code == 2

    def test_run_cli_two_file_invalid_keep_going_on(self):
        args = [
            "-k",
            "-f",
            str(DATA_INVALID / "1"),
            "-f",
            str(DATA_INVALID / "2"),
        ]
        out = self.cli_runner.invoke(leodagan, args=args)
        assert out.exit_code == 1
