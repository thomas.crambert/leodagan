import pytest
from click.testing import CliRunner

from leodagan.logger import logger

logger.setLevel("DEBUG")


@pytest.fixture()
def cli_runner():
    return CliRunner(mix_stderr=False)
