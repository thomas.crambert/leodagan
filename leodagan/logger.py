import logging

import click
from rich.console import Console
from rich.logging import RichHandler

logger: logging.Logger = logging.getLogger("leodagan")
handler = RichHandler(
    console=Console(stderr=True),
    show_path=False,
    rich_tracebacks=True,
    tracebacks_suppress=[click],
)
handler.setFormatter(
    logging.Formatter("[%(name)s]: %(message)s", " ")
)  # the second argument disables dateformat
logger.addHandler(handler)
