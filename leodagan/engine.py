"""
    The engine part making the link between the input and the rules
"""

from email.header import decode_header
from typing import List

import leodagan.rules as lr
from leodagan.logger import logger

from .exceptions import Leodagan


class News:
    """
    Represent a news
    """

    @staticmethod
    def parse_headers(lines: list) -> dict:
        """
        Parse the headers of a news
        """
        headers = {}

        for i in range(len(lines)):
            line = lines[i]
            if line == "":  # end of headers
                break
            if line[0] == " " or line[0] == "\t":
                # Multiline headers already handled
                continue
            header = line.split(": ", 1)

            if len(header) == 1:
                headers[header[0].lower()] = ""
                continue

            # Handle multiline headers
            peek = 1
            while True:  # Do while
                try:
                    if lines[i + peek][0] == " ":
                        header[1] += " " + lines[i + peek]
                        peek += 1
                    else:
                        break
                except IndexError:
                    break

            # Handle value like =?UTF-8?Q?=5bINFR=5d=5bMAINTENANCE=5d_Coupure?=
            decoded_el = decode_header(header[1])
            value = ""
            for el in decoded_el:
                if el[1] is None and isinstance(el[0], str):
                    # No encoding used
                    value += el[0]
                elif el[1] is not None:
                    # Decode using given encoding
                    value += el[0].decode(el[1], errors="ignore")
                else:
                    value += el[0].decode("utf-8", errors="ignore")
            headers[header[0].lower()] = value
        return headers

    def __init__(self, content):
        self.content = content
        self.lines = content.split("\n")
        self.headers = News.parse_headers(self.lines)
        self.body = content[content.find("\n\n") + 2 :]

    def __str__(self):
        return str(self.headers) + "\n\n" + self.body


def check_wrapper(func, *args):
    """
    A wrapper of checker to catch the exceptions
    """
    rules = []
    try:
        func(*args)
        logger.debug(f"{func} checked")
    except Leodagan as err:
        rules.append(err)
    return rules


def check_wrapper_iter(func, *args) -> List[Leodagan]:
    """
    A wrapper of checker to iter of rule-generator
    """
    rules = []
    gen = iter(func(*args))
    try:
        while True:
            try:
                obj = gen.__next__()
                if obj is not None:
                    raise obj
            except Leodagan as err:
                rules.append(err)
    except StopIteration:
        return rules
    return rules


def process_news(content: str, identification: str = None) -> List[Leodagan]:
    """
    Process a news with its content and apply the rules
    """
    try:
        news = News(content)
    except Exception as err:  # FIXME
        logger.error(f"Cannot parse content : {str(err)}")
        return []

    rules = []

    rules += check_wrapper_iter(lr.check_subject, news.headers.get("subject"))
    rules += check_wrapper(lr.check_basic_body_formatting, news.body)
    rules += check_wrapper_iter(lr.check_max_cols, news.body)
    rules += check_wrapper(lr.check_signature, news.body)
    rules += check_wrapper(lr.check_quoting, news.body)

    user_from = news.headers.get("from", None)
    if user_from is None:
        logger.warning(f"Could not get author from news {identification}")
    else:
        logger.debug(f"{user_from} / {identification}")

    return rules
