from typing import List, Optional

import leodagan.engine as lengine
from leodagan.logger import logger

from .exceptions import Leodagan


def check_file(file: str, keep_going: bool) -> Optional[List[Leodagan]]:
    """
    Run léodagan on the given files
    """
    logger.debug(f"Processing file {file}...")
    try:
        with open(file, "r") as f:
            content = f.read()
    except (FileNotFoundError, PermissionError, UnicodeDecodeError, IsADirectoryError):
        logger.error(f"Error while processing file {file}, aborting")
        return None if not keep_going else []

    return lengine.process_news(content)
