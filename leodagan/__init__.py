import logging
import sys
from typing import Any, Callable, List

import click

from leodagan.logger import logger

from .file import check_file

try:
    import click_completion  # type: ignore

    click_completion.init()
except ImportError:
    logger.debug("click_completion not installed, skipping…")


def option(*args, **kwargs) -> Callable[..., Any]:
    """
    option decorator with defaults values
    """
    return click.option(*args, show_default=True, show_envvar=True, **kwargs)


@click.command()
@click.option(
    "--verbosity",
    "-v",
    default="INFO",
    help="""Verbosity level for the loggers, either
    CRITICAL, ERROR, WARNING, INFO or DEBUG""",
)
@click.option(
    "--only-fail",
    is_flag=True,
    default=False,
    help="Only list files which failed to pass the checks",
)
@click.option(
    "-k",
    "--keep-going",
    is_flag=True,
    default=False,
    help="Do not stop at first error, enabled by default",
)
@click.option(
    "-f",
    "--files",
    multiple=True,
    is_flag=False,
    help="files to check",
    default=[],
)
def leodagan(verbosity: str, only_fail: bool, keep_going: bool, files: List[str]):
    ret_code = 0
    level = getattr(logging, verbosity, None)
    if level is None:
        raise click.BadParameter(
            f"Must be CRITICAL, ERROR, WARNING, INFO or DEBUG, not {verbosity}"
        )
    logger.setLevel(level)
    for f in files:
        logger.info(f"Checking file {f}")
        rules = check_file(f, keep_going)
        if rules is None:
            logger.warning(f"Error occured while checking file {f}")
            if not keep_going:
                sys.exit(1)
        elif len(rules) == 0:
            if not only_fail:
                logger.info(f"File {f} is netiquette compliant")
        else:
            for r in rules:
                logger.warning(str(r))
            logger.error(f"File {f} is not netiquette compliant")
            ret_code = 1
            if not keep_going:
                sys.exit(2)
    sys.exit(ret_code)
