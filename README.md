# Introduction

This repository is merely a fork of https://gitlab.cri.epita.fr/cyril/leodagan.
This fork aims to modernize the tool and make it easier of use for students.

Compared to the original:
- [poetry](https://python-poetry.org/) is used as the build system
- the CLI uses [click](https://click.palletsprojects.com/en/8.1.x/)
- logging is done with [rich](https://rich.readthedocs.io/en/stable/introduction.html)

# Dependencies

Only `python` and `poetry` are required on your system beforehand.

# Installing

```
git clone https://gitlab.cri.epita.fr/thomas.crambert/leodagan
cd leodagan
poetry install
```

And then run `poetry shell` would open you a new shell with the command `leodagan` available

# Usage

```
Usage: leodagan [OPTIONS]

Options:
  -v, --verbosity TEXT  Verbosity level for the loggers, either CRITICAL,
                        ERROR, WARNING, INFO or DEBUG
  --only-fail           Only list files which failed to pass the checks
  -k, --keep-going      Do not stop at first error, enabled by default
  -f, --files TEXT      files to check
  --help                Show this message and exit.
```

Assuming your current directory is the root of this repository,
```sh
leodagan -k -f tests/non-valid/1 -f tests/non-valid/2
```
Should give the output
```
  INFO     [leodagan]: Checking file tests/non-valid/1
  WARNING  [leodagan]: Invalid message body: Line 3 width exceeding 80 chars / see 2.2.2.1
  WARNING  [leodagan]: Invalid message body: Line 4 width exceeding 80 chars / see 2.2.2.1
  WARNING  [leodagan]: Invalid message body: Line 5 width exceeding 80 chars / see 2.2.2.1
  WARNING  [leodagan]: Invalid message body: Line 6 width exceeding 80 chars / see 2.2.2.1
  WARNING  [leodagan]: Invalid message body: Line 7 width exceeding 72 chars without quoting / see 2.2.2.1
  ERROR    [leodagan]: File tests/non-valid/1 is not netiquette compliant
  INFO     [leodagan]: Checking file tests/non-valid/2
  WARNING  [leodagan]: Invalid subject: Length exceed 80 chars / see 2.1.1.2
  WARNING  [leodagan]: Invalid subject: Subject must have tags and a summary / see 2.1.1
  WARNING  [leodagan]: Invalid subject: Subject cannot have only one tag / see 2.1.1
  ERROR    [leodagan]: File tests/non-valid/2 is not netiquette compliant
```

It is also possible to run this tool from poetry, the command would then become,
```
poetry run leodagan -k -f tests/non-valid/1 -f tests/non-valid/2
```

Be careful, you must be at the same level as the `pyproject.toml` if you decide to use it this way.
